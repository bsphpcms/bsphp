<?php

$daihao = PLUG_DAIHAO(); //软件代号
$type = Plug_Set_Data('type'); //类别
$v = Plug_Set_Data('v'); //版本号
$number = Plug_Set_Data('number'); //软件版本号

if ($daihao == '') {
    Plug_Echo_Info('软件代号异常');
    exit;
}

if ($type == '') {
    Plug_Echo_Info('软件类别异常');
    exit;
}

if ($number == 0) {
    Plug_Echo_Info('软件数字版本号不能空');
    exit;
}

#查询最新一条
$sql = "SELECT * FROM `plug_bsphp_edition` WHERE `type` = '$type' and `daihao`='$daihao' and `number_v` > '$number' ORDER BY `plug_bsphp_edition`.`id` DESC";
$db_array_value = Plug_Query($sql);

$while_array_list_all = array();
while ($array = mysqli_fetch_assoc($db_array_value)) {
    $array['add_time'] = date('Y-m-d H:i:s', $array['add_time']);
    $while_array_list = array("code" => 200, 'msg' => call_my_Lang("有版本更新"),'upforce' => $array['upforce'], 'md5' => $array['md5'], 'v' => $array['v'], 'number_v' => $array['number_v'], 'upinfo' => $array['upinfo'], 'info' => $array['info'], 'time' => $array['add_time'], 'url' => $array['down_url']);
    $while_array_list_all[] = $while_array_list;
}



//固定格式
$json_array = array();
$json_array['list'] =  $while_array_list_all;
$json_array['code'] = 100;
$json_array['msg'] = '获取成功';
$json_array['rows'] = count($while_array_list_all);
Plug_Echo_Info(json_encode($json_array,JSON_UNESCAPED_UNICODE));


