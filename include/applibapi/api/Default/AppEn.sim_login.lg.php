<?php

#登录功能



/**
 * **********************接口介绍说明******************************************
 * sim_login.lg
 * 用户登录
 * *****************************************************************************
 */
#预设好文本字符串数组
$user_str_log = plug_load_langs_array("" . 'user', "" . 'user_str_log');
$appen_str_log = plug_load_langs_array('applib', 'appen_str_log');


$daihao = PLUG_DAIHAO();
$user = Plug_Set_Data('user');        #登录账号
$user_pwd = Plug_Set_Data('pwd');     #登录密码
$key = Plug_Set_Data('key');          #绑定特征,如果已经解除绑定会登录自动绑定
$maxoror = Plug_Set_Data('maxoror');  #控制多开机器数量机器码/唯一码


$BSphpSeSsL = Plug_Set_Data('BSphpSeSsL');


//--------------------------------短信验证码开始
$code = Plug_Set_Data('coode');
$unionid =  plug_get_session_value('unionid');
$Mobile = $user;
$Mobile_s = plug_get_session_value('coode_mobile');
$code_s = plug_get_session_value('coode_code');
$codeunm = plug_get_session_value('coode_error_unm');

if ($Mobile == '') {
    plug_PrInfo(Plug_Lang("请输入手机号码！"), -11);
}

if ($code == '') {
    plug_PrInfo(Plug_Lang("请输入验证码！"), -12);
}

if ($code != $code_s) {
    plug_get_session_value('coode_error_unm', $codeunm++);
    plug_PrInfo(Plug_Lang("短信验证码错误！"), -13);
}
if ($codeunm > 5) {
    plug_get_session_value('code', time());
    plug_PrInfo(Plug_Lang("短信验证码错误次数过多请重发验证码！"), -14);
}

if ($Mobile !== $Mobile_s) {
    plug_PrInfo(Plug_Lang("提交手机号码与验证码的不符！"), -15);
}

///------------------------短信结束






// //验证session是否正常，或者被踢出的需要更换BSphpSeSsL
// if(!Plug_Is_Session_Ok($BSphpSeSsL)){
//     Plug_Echo_Info('系统检测到SeSsL需要更换新.', -1092);
// }



if (Plug_App_DaTa('app_MoShi') !== 'LoginTerm') {

    Plug_Echo_Info($appen_str_log[5057], 5057);
}


if ($user == '') {
    Plug_Echo_Info('请输入账号.', -1);
}






$uid = Plug_Query_One('bs_php_user', 'user_Mobile', $user, '`user_uid`');
$log == myuidlogin_($uid);


//登陆前检查绑定--------------------------------------------------------------------------------
if ($log == 1011) { //登录成功
    //读取用户配置
    $uid = Plug_Query_One('bs_php_user', 'user_user', $user, '`user_uid`');

    /*查询是否重复绑定*/
    if (Plug_App_DaTa('app_key_zhong') == 1) {

        //查询该key是否已经在数据库
        $sql = "SELECT*FROM`bs_php_pattern_login`WHERE`L_daihao`='{$daihao}'AND`L_key_info`= '{$key}' LIMIT 1;";
        $zhong_arr = Plug_Query_Assoc($sql);


        if (!$zhong_arr) {
            $arr = Plug_Get_App_User_Info($uid, $daihao);
            //判断是否已经绑定过了
            if ($arr['L_key_info'] == '') {
                //添加绑定
                $sql = "UPDATE`bs_php_pattern_login`SET`L_key_info`='$key'WHERE  `L_id`='{$arr['L_id']}';";
                $tmp = Plug_Query($sql);
            }
        }
    } else {  #可重复绑定

        $arr = Plug_Get_App_User_Info($uid, $daihao);
        //判断是否已经绑定过了
        if ($arr and $arr['L_key_info'] == '') {
            //添加绑定
            $sql = "UPDATE`bs_php_pattern_login`SET`L_key_info`='$key'WHERE  `L_id`='{$arr['L_id']}';";
            $tmp = Plug_Query($sql);
        }
    }
}
//end
//------------------------------------------------------------------------------

$uid = Plug_Query_One('bs_php_user', 'user_Mobile', $user, '`user_uid`');
$log == myuidlogin_($uid);



if ($log == 1011) { #登录成功


    //短信验证码重设
    plug_get_session_value('code', time());


    $uid = Plug_Query_One('bs_php_user', 'user_user', $user, '`user_uid`');








    #获取软件用户信息
    $arr = Plug_Get_App_User_Info($uid, $daihao);



    #没有使用过的话就给程序添加上
    if (!$arr) {


        $date = (int)Plug_App_DaTa('app_re_date'); //获取赠送时间

        $date = PLUG_UNIX() + $date;

        /*查询是否重复绑定*/
        //查询该key是否已经在数据库
        $sql = "SELECT*FROM`bs_php_pattern_login`WHERE`L_daihao`='{$daihao}'AND`L_key_info`= '{$key}' LIMIT 1;";
        $zhong_arr = Plug_Query_Assoc($sql);
        if ($zhong_arr  and Plug_App_DaTa('app_key_zhong') == 1) {
            Plug_Echo_Info('[5009]绑定特征码,已经有人绑定过了,不能重复绑定,不能登陆', 5009); //'绑定特征码,已经有人绑定过了,不能重复绑定,不能登陆'
        }

        if (Plug_App_Login_Add_Key($uid, $daihao, $date, $key, $user, $user)) {

            $arr = Plug_Get_App_User_Info($uid, $daihao);
        } else {
            Plug_Echo_Info('注册新用户失败,请联系管理员！', -200);
        }
    }


    if ($arr['L_IsLock'] > 0) {
        Plug_Echo_Info('当前账号已经被冻结禁止登录当前软件.', -200);
    }


    /**
     * 更新登录模式下最后登陆日期
     */
    $date = PLUG_DATE();
    $sql = "UPDATE`bs_php_pattern_login`SET`L_login_time`='$date'WHERE`bs_php_pattern_login`.`L_User_uid`='$uid'AND`bs_php_pattern_login`.`L_daihao`='$daihao';";
    Plug_Query($sql);

    #建立登录限制
    $log = Plug_Login_Multi_Control($user, $daihao, $maxoror, $uid);
    if ($log != 5047)
        Plug_Echo_Info($appen_str_log[$log], $log);



    #日志记录
    Plug_Add_AppenLog('user_login_log', '软件登录', $user);



    //获取用户信息赋值给变量
    $uesr_key = $arr['L_key_info'];
    $uesr_vipdate = $arr['L_vip_unix'];
    $login_info = null;
    if ($key == $uesr_key & $uesr_key != '' or Plug_App_DaTa('app_set') == 0)
        $login_info = Plug_App_DaTa('app_logininfo');


    //链接数验证


    //更新在线用户列表信息
    Plug_Links_Add_Info($uid, $user, $key, $daihao, $maxoror);


    //-----------------------------------------


    /**
     * 返回数据说明
     */
    if ($arr['L_vip_unix'] > PLUG_UNIX()) {

        //登陆成功  或者在免费使用


        if (Plug_App_DaTa('app_set') == 1) {


            if ($arr['L_key_info'] !== $key) {
                //注销登录
                Plug_Set_Session_Value('USER_UID', ''); //登陆UID
                Plug_Set_Session_Value('USER_YSE', ''); //登陆MD7加密
                Plug_Set_Session_Value('USER_DATE', ''); //上一次登陆时间
                Plug_Set_Session_Value('USER_IP', ''); //上一次登陆IP
                Plug_Set_Session_Value('USER_MD7', ''); //OOKIE MD7验证串
                //Plug_Links_Out_Session_Id($BSphpSeSsL); 注销不能用会注销锁定sessl

                if ($arr['L_key_info'] == '') Plug_Echo_Info("还没有绑定,请绑定在登录", -3);





                Plug_Echo_Info('[5035]' . $appen_str_log[5035], 5035);
            }
        }


        //---------------------------------------


        //链接数验证

        //-----------------------------------------
        //记录登录时间用做扣点
        $UNIX = PLUG_UNIX();
        $sql = "UPDATE`bs_php_pattern_login`SET`L_timing`='$UNIX'WHERE`bs_php_pattern_login`.`L_User_uid`='$uid'AND`bs_php_pattern_login`.`L_daihao`='$daihao';";
        Plug_Query($sql);


        $uesr_vipdate = date('Y-m-d H:i:s', $uesr_vipdate);
        /**
         * 返回说明
         * 1.= 成功返回1
         * 2.= 登陆成功代号
         * 3.= 用户绑定key
         * 4.= 用户登陆成功返回特定数据
         * 5.= VIP到期时间
         */
        Plug_Echo_Info("01|1011|$uesr_key|$login_info|$uesr_vipdate|||||", 1011);
    } else {

        Plug_Echo_Info('[9908]使用已经到期.', 9908);
    }
}

Plug_Echo_Info($user_str_log[$log], $log);




#登陆功能函数
function myuidlogin_($uid)
{

    $AGENT = 0;
    $DATE = date("Y-m-d H:i:s");
    $IP = $_SERVER['REMOTE_ADDR'];
    $UNIX = time();


    $sql = "SELECT`user_uid`,`user_user`,`user_pwd`,`user_IsLock`,`user_LoGinNum`,`user_Login_ip`,`user_Login_date`,`user_daili`,`user_email`FROM`bs_php_user`WHERE  `user_uid`='$uid' ";
    $arr = plug_query_array($sql);
    if (!$arr)
        return 1010;




    if ($arr['user_IsLock'] > 0) {
        return 1021;
    }

    if ($arr['user_IsLock'] == 1) {
        return 1021;
    }



        $uid = $arr['user_uid'];
        $sql = "UPDATE`bs_php_user`SET`user_DenJi_tmp`='$UNIX',`user_Login_ip`='$IP',`user_Login_date`='$DATE',`user_CaoShi`='$UNIX',`user_LoGinNum`=`user_LoGinNum`+1 WHERE`bs_php_user`.`user_uid`='$uid';";
        $tmp = plug_query($sql);
        $sen = 'USER';
        $cookies_pwd = cookies_md7($arr['user_pwd']);
        $cookies_ip = $arr['user_Login_ip'];
        $cookies_DATE = $arr['user_Login_date'];
        $LoGinNum = $arr['user_LoGinNum'];
        $LoGinNum = 'ALL';
        $cookies_md7 = cookies_md7($uid . $cookies_pwd . $cookies_DATE . $cookies_ip . $LoGinNum);
        plug_set_session_value("USER_UID", $uid);
        plug_set_session_value('USER_YSE', $cookies_pwd);
        plug_set_session_value('USER_DATE', $cookies_DATE);
        plug_set_session_value('USER_IP', $cookies_ip);
        plug_set_session_value('USER_MD7', $cookies_md7);
        addappenlog('user_login_log','用户网页面板三方登陆',strtolower($arr['user_user']));


        #添加软件
        $sql = "SELECT * FROM  `bs_php_user` WHERE  `user_uid`='$uid'";
        $User_Info = plug_query_array($sql);
        
    	$re_daihao  = plug_get_configs_value('plug_plug_user', 're_daihao');
    	$re_daihaoARR = explode('|',$re_daihao);
    	
    
    	foreach ($re_daihaoARR as $v){
    		
            $sql = "SELECT * FROM`bs_php_pattern_login` where `L_User_uid` ='{$User_Info['user_uid']}' and `L_daihao`='$v';";
            $AAAA = plug_query_array($sql);
            
       
            if (!$AAAA) {
              
              
              #读取软件配置
              $sql = "SELECT * FROM`bs_php_appinfo` where  `app_daihao`='$v';";
              $app = plug_query_array($sql);
              
              if($app['app_MoShi']=='LoginTerm' or $app['app_MoShi']=='CardTerm'){
              	Login_add_key($User_Info['user_uid'], $v, (time()+$app['app_re_date']), '','',$User_Info['user_user']);
              }else{
              	Login_add_key($User_Info['user_uid'], $v, $app['app_re_date'], '','',$User_Info['user_user']);
              }
              
              
              
            }
            
            
    	}
    	
        return 1011;

}
