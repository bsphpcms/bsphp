<?php

#获取短信验证码
#新版本sdk过度臃肿,用老版本文件少.
//http://app.bsphp.com/AppEn.php?appid=8888888&m=95e87faf2f6e41babddaef60273489e1&api=BSphpSeSsL.in&date=2018-12-2113:14:12&md5=&mutualkey=6600cfcd5ac01b9bb3f2460eb416daa8&appsafecode=12345
$bin_time =  plug_get_session_value('bin_time');
if (time() - $bin_time < 5) {
    plug_set_session_value('bin_time', time());
    plug_PrInfo(Plug_Lang("你手速太快了"));
    //plug_PrInfo(array("code" => -11, 'msg' => Plug_Lang("你手速太快了")));
}

$user = plug_set_data('user');
$type = plug_set_data('type');

if ($user == '') {
    plug_PrInfo(Plug_Lang("手机号不能为空"));
    //plug_PrInfo(array('code' => -2, 'msg' => Plug_Lang('手机号不能为空')));
}


if ($type == '') {
    $type = '86';
    if (strlen($user) != 11) {
        plug_PrInfo(Plug_Lang("请输入11位手机号码"));
        //plug_PrInfo(array('code' => -2, 'msg' => Plug_Lang('请输入11位手机号码')));
    }
}



$code = rand(1000, 9999);

$time = plug_get_session_value('coode_sendtime');
$coode_mobile = plug_get_session_value('coode_mobile');
$time = time() - $time;
if ($time < 120 and $coode_mobile == $user) {
    $time2 = 120 - $time;

    plug_PrInfo(Plug_Lang(Plug_Lang('验证码已经发送至') . $user . Plug_Lang('手机,不要频繁发送 倒计时') . $time2 . Plug_Lang('秒在发送')));
    //plug_PrInfo(array('code' => -1, 'msg' => Plug_Lang('验证码已经发送至') . $user . Plug_Lang('手机,不要频繁发送 倒计时') . $time2 . Plug_Lang('秒在发送')));
}


plug_set_session_value('coode_md5', md5($code));
plug_set_session_value('coode_code', $code);
plug_set_session_value('coode_error_unm', 0); //输入错误次数
plug_set_session_value('coode_type', 'all');
plug_set_session_value('coode_mobile', $user);
plug_set_session_value('coode_sendtime', time());
plug_set_session_value('coode_error_unm', 1);
plug_set_session_value('coode_error_unm', 0);






require plug_get_bsphp_dir() . "/Plug/Plug_List/plug_simcode/qcloudsms_php-master/src/index.php";

use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsVoiceVerifyCodeSender;
use Qcloud\Sms\SmsVoicePromptSender;
use Qcloud\Sms\SmsStatusPuller;
use Qcloud\Sms\SmsMobileStatusPuller;
use Qcloud\Sms\VoiceFileUploader;
use Qcloud\Sms\FileVoiceSender;
use Qcloud\Sms\TtsVoiceSender;
// 短信应用SDK AppID
$appid = plug_get_configs_value("plug_plug_simcode", "tx_appid"); // 1400开头

// 短信应用SDK AppKey
$appkey = plug_get_configs_value("plug_plug_simcode", "tx_appkey");;
// 需要发送短信的手机号码
$phoneNumbers = array($user);
// 短信模板ID，需要在短信应用中申请
$templateId = plug_get_configs_value("plug_plug_simcode", "tx_appcode");;  //483861 NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请
// 签名
$smsSign = plug_get_configs_value("plug_plug_simcode", "tx_codename");; // NOTE: 这里的签名只是示例，请使用真实的已申请的签名，签名参数使用的是`签名内容`，而不是`签名ID`


$ssender = new SmsSingleSender($appid, $appkey);
$params = array($code);
if ($type == '86') {
    $result = $ssender->sendWithParam(
        "$type",
        $phoneNumbers[0],
        $templateId,
        $params,
        $smsSign,
        "",
        ""
    );
} else {



    //国际签名
    $templateId = plug_get_configs_value("plug_plug_simcode", "tx_gappcode");;  //483861 NOTE: 这里的模板ID`7839`只是一个示例，真实的模板ID需要在短信控制台中申请
    // 签名
    $smsSign = plug_get_configs_value("plug_plug_simcode", "tx_gcodename");; // NOTE: 这里的签名只是示例，请使用真实的已申请的签名，签名参数使用的是`签名内容`，而不是`签名ID`

    $result = $ssender->sendWithParam(
        "$type",
        $phoneNumbers[0],
        $templateId,
        $params,
        $smsSign,
        "",
        ""
    );
}

// 签名参数未提供或者为空时，会使用默认签名发送短信
$rsp = json_decode($result);
//echo $result;

if ($rsp->errmsg == 'OK') {
    plug_PrInfo(Plug_Lang('验证码已经发送至') . $user . Plug_Lang('手机'), 200);
    //  plug_PrInfo(array('code' => 200, 'msg' => Plug_Lang('验证码已经发送至') . $user . Plug_Lang('手机')), 200);
} else {
    plug_PrInfo(Plug_Lang($result), -1);
    // plug_PrInfo(array('code' => -1, 'msg' => $result));
}
