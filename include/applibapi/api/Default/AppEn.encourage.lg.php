<?php



#插件功能,通过奖励发放日志. 用户注册送时间检测接口
#用法用户注册/登录成功执行该接口.即可给邀请他用户发佣金



if (Plug_App_DaTa('app_MoShi') == 'CardTerm' or Plug_App_DaTa('app_MoShi') == 'CardPoint') {
    Plug_Echo_Info('当前卡模式下软件不支持该接口调用');
}


#注册超过多少天不检测
$reday = 7;


#送时间 单位秒/点
$vip_date = 86400;


#当前软件编号
$daihao = PLUG_DAIHAO();


#api参数用户帐号
$user =  Plug_Set_Data('user');
if (!$user) {
    Plug_Echo_Info('帐号参数不能为空,参数名user');
}


#读取被邀请人
$sql = "SELECT * FROM `bs_php_user` WHERE `user_user` LIKE '{$user}' ";
$user_array = Plug_Query_Assoc($sql);
if (!$user_array) {
    Plug_Echo_Info('帐号不存在 ' . $user);
}


#过滤N天前的邀请,防止删除日志在次领取
$user_re_date = strtotime($user_array['user_re_date']);
$_s = 86400 * $reday;
$_s = PLUG_UNIX() - $_s;
if ($user_re_date > $_s) {
   Plug_Echo_Info('注册超' . $reday . '天,已经反现奖励');
}


#获取奖励人
if ($user_array['user_yao_User'] == '') {
    Plug_Echo_Info('没有邀请人,无需发放');
}
$to_user = $user_array['user_yao_User'];




#读取奖励日志
$sql = "SELECT * FROM `bs_php_log` WHERE `leixing` LIKE 'yao_registration_log' AND `test` LIKE '%[邀请奖励] %' AND `user` LIKE '$to_user'";
$bs_php_log = Plug_Query_Assoc($sql);
if ($bs_php_log) {
    Plug_Echo_Info('已经领取,无需在次领取!');
}



#读取被奖励使用软件情况
$sql = "SELECT * FROM `bs_php_pattern_login` WHERE `L_ic_name` LIKE '$to_user' AND `L_daihao`='$daihao' ";
$yao_app = Plug_Query_Assoc($sql);
if (!$yao_app) {
    Plug_Echo_Info('被邀请用户没有使用软件');
}






#判断软件是点还是时间
if (Plug_App_DaTa('app_MoShi') == 'LoginTerm') {



    #判断有没有到期
    if ($yao_app['L_vip_unix'] > PLUG_UNIX()) {

        #没过期直接累加
        $L_vip_unix = $yao_app['L_vip_unix'] + $vip_date;
    } else {

        $L_vip_unix = PLUG_UNIX() + $vip_date;
    }
} else {
    #点模式直接累加
    $L_vip_unix = $yao_app['L_vip_unix'] + $vip_date;
}




#发放奖励
$sql = "UPDATE `bs_php_pattern_login` SET `L_vip_unix` = '$L_vip_unix' WHERE `bs_php_pattern_login`.`L_id` = '{$yao_app['L_id']}';";
$tmp = Plug_Query($sql);
if ($tmp) {


    Plug_Add_AppenLog('yao_registration_log', "[邀请奖励] 邀请:{$user} 时间:{$vip_date}秒/点", $to_user);


    Plug_Echo_Info('奖励发放成功');
} else {
    Plug_Echo_Info('奖励发放失败');
}
