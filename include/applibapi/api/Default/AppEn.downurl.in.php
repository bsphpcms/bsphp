<?php

$daihao = PLUG_DAIHAO(); //自动获取软件代号
$type = Plug_Set_Data('type'); //类别

if ($daihao == '') {
    Plug_Echo_Info('软件代号异常');
    exit;
}

if ($type == '') {
    Plug_Echo_Info('软件类别异常');
    exit;
}

#查询最新一条
$sql = "SELECT * FROM `plug_bsphp_edition` WHERE `type` = 'pc' and `daihao`='$daihao' ORDER BY `plug_bsphp_edition`.`id` DESC";
$array = Plug_Query_Assoc($sql);
if (!$array) {
    Plug_Echo_Info('没有查询到版本信息,请确认参数输入正确');
    exit;
}

//更新调用次数
$api_time = time();
$sql = "UPDATE `plug_bsphp_edition` SET `number_api` = `number_api` + '1',`api_time`='$api_time' WHERE `plug_bsphp_edition`.`id` = '{$array['id']}';";
Plug_Query($sql);


#直接跳转
Plug_Echo_Info($array['down_url'], 800);
exit;
