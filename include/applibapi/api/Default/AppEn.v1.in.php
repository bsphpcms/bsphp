<?php


$daihao = PLUG_DAIHAO(); //软件代号
$type = Plug_Set_Data('type'); //类别
$v = Plug_Set_Data('v'); //版本号
$number = Plug_Set_Data('number'); //软件版本号


if ($daihao == '') {
    Plug_Echo_Info('软件代号异常');
    exit;
}

if ($type == '') {
    Plug_Echo_Info('软件类别异常');
    exit;
}

if ($v == '') {

    Plug_Echo_Info('软件版本不能空');
    exit;
}


#查询最新一条
$sql = "SELECT * FROM `plug_bsphp_edition` WHERE `type` = '$type' and `daihao`='$daihao' ORDER BY `plug_bsphp_edition`.`id` DESC";
$array = Plug_Query_Assoc($sql);
if (!$array) {

    Plug_Echo_Info(json_encode(array("code" => 0, 'msg' => '没有查询到版本信息,请确认参数输入正确'),JSON_UNESCAPED_UNICODE));
    exit;
}

$array['add_time'] = date('Y-m-d H:i:s', $array['add_time']);

if ($array['v'] == $v) {

    Plug_Echo_Info(json_encode(array("code" => 800,'msg' => call_my_Lang("版本最新的")),JSON_UNESCAPED_UNICODE));


} else {

    //更新调用次数
    $api_time = time();
    $sql = "UPDATE `plug_bsphp_edition` SET `number_api` = `number_api` + '1',`api_time`='$api_time' WHERE `plug_bsphp_edition`.`id` = '{$array['id']}';";
    Plug_Query($sql);

    Plug_Echo_Info(json_encode(array("code" => 200, 'msg' => call_my_Lang("有版本更新"),'upforce' => $array['upforce'], 'md5' => $array['md5'], 'v' => $array['v'], 'number_v' => $array['number_v'], 'upinfo' => $array['upinfo'], 'info' => $array['info'], 'time' => $array['add_time'], 'url' => $array['down_url']),JSON_UNESCAPED_UNICODE));
}
