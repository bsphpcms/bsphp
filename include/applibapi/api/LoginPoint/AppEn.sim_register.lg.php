<?php

#注册账号




/***********************接口介绍说明******************************************
 * registration.lg
 * 注册账号
 * *****************************************************************************
 */
#预设好文本字符串数组
$user_str_log = plug_load_langs_array("" . 'user', "" . 'user_str_log');
$appen_str_log = plug_load_langs_array('applib', 'appen_str_log');

$daihao = PLUG_DAIHAO();


$user = Plug_Set_Data('user');       #注册账号
$pwd = Plug_Set_Data('pwd');         #注册登录密码
$pwdb = Plug_Set_Data('pwdb');       #再次输入密码
$qq = Plug_Set_Data('qq');           #联系qq
$mail = Plug_Set_Data('mail');       #联系邮箱
$key = Plug_Set_Data('key');          #绑定特征/机器码
$img = Plug_Set_Data('img');          #开启验证码接到的验证码



$Mobile = Plug_Set_Data('mobile');    #联系电话
$user_mibao_wenti = Plug_Set_Data('mibao_wenti');    #密保问题
$user_mibao_daan = Plug_Set_Data('mibao_daan');      #密保答案
$extension = Plug_Set_Data('extension');    #邀请人UID




//--------------------------------短信验证码开始
$code = Plug_Set_Data('coode');
$unionid =  plug_get_session_value('unionid');
$Mobile = $user;
$Mobile_s = plug_get_session_value('coode_mobile');
$code_s = plug_get_session_value('coode_code');
$codeunm = plug_get_session_value('coode_error_unm');

if ($Mobile == '') {
    plug_PrInfo(Plug_Lang("请输入手机号码！"), -11);
}

if ($code == '') {
    plug_PrInfo(Plug_Lang("请输入验证码！"), -12);
}

if ($code != $code_s) {
    plug_get_session_value('coode_error_unm', $codeunm++);
    plug_PrInfo(Plug_Lang("短信验证码错误！"), -13);
}
if ($codeunm > 5) {
    plug_get_session_value('code', time());
    plug_PrInfo(Plug_Lang("短信验证码错误次数过多请重发验证码！"), -14);
}

if ($Mobile !== $Mobile_s) {
    plug_PrInfo(Plug_Lang("提交手机号码与验证码的不符！"), -15);
}


#防止判断注册账号使用手机占用他人登录,登录账号 手机 uid 判断
if (strlen($user) == 11) {
    if ($user > 0 and $user != $Mobile) {
        plug_PrInfo(Plug_Lang("注册账号需要包含字母数字一起."), -20);
    }
}


$uid = Plug_Query_One('bs_php_user', 'user_Mobile', $user, '`user_uid`');
if ($uid > 0) {
    plug_PrInfo(Plug_Lang("当前手机号已经有注册,请直接登录即可."), -222);
}

///------------------------短信结束


/**
 * 判断检测账号重复
 */

$sql = "SELECT*FROM`bs_php_pattern_login`WHERE`L_daihao`='$daihao'AND`L_key_info`LIKE '$key';";
$tmp = Plug_Query_Assoc($sql);

if (Plug_App_DaTa('app_key_zhong') == 1 & $tmp) {


    Plug_Echo_Info('[5010],当前账号已经注册过,无法在同一环境注册多个账号', 5010);
}


/**
 * 判断邀请注册IUD是否存在
 */
if ($extension > 0) {
    $uid_extension = Plug_Query_One('bs_php_user', 'user_uid', $extension, '`user_uid`');
    if (!$uid_extension)
        Plug_Echo_Info('推荐人错误,没有请不要填写！', -2);
}

/**
 * 用户注册
 */
$log = Plug_User_Re_Add($user, $pwd, $pwdb, $qq, $mail, $extension, $Mobile, $user_mibao_wenti, $user_mibao_daan);
if ($log == 1005) {


    $date = (int)Plug_App_DaTa('app_re_date'); //获取赠送时间


    /*查询是否重复绑定*/
    $uid = Plug_Query_One('bs_php_user', 'user_user', $user, '`user_uid`');




    Plug_App_Login_Add_Key($uid, $daihao, $date, $key, $user, $user); //添加特征


    //短信验证码重设
    plug_get_session_value('code', time());

    if ((int)Plug_App_DaTa('app_re_date') == 0) {

        Plug_Echo_Info('注册成功', 1005);
    } else {
        $s = Plug_App_DaTa('app_re_date') / 3600;
        Plug_Echo_Info('注册成功,恭喜你获得了' . $date . '点使用期限', 1005);
    }
}
Plug_Echo_Info($user_str_log[$log], $log);
