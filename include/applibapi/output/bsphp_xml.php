<?php

/**
 * 输出文件
 * 
 *  * 文件名_bsphp_output(显示内容)  输出显示函数
 */


/**
 * @输出给客户端mxl格式文件
 */
function bsphp_xml_bsphp_output($data, $code)
{


    if (is_array($data)) {
        //参数修改
        $code_array_php = '';
        foreach ($data as $param_value => $key) {
            $key = addslashes($key);
            $code_array_php .= "<$param_value>$key</$param_value>";
        }
        $data = $code_array_php;
    }

    $microtime = Plug_Execute_Time();
    $sginstr = str_replace('[KEY]', $data . PLUG_DATE() . PLUG_UNIX() . $microtime . Plug_Set_Data('appsafecode'), Plug_App_DaTa('app_tosgin'));
    $sgin_md5 = md5($sginstr);

    $str = iconv("UTF-8", "gbk//TRANSLIT", $sginstr);
    $sgin_md52 = md5($str);


    #签名调试记录日志
    $sgin_md5all = "Utf8:{$sgin_md5}/GBK:{$sgin_md52}";
    $key = Plug_App_DaTa('app_tosgin');
    Plug_Dbug_update("`to_sigm_key`='{$key}',`to_sigm_txt` = '{$sginstr}',`to_sigm_md5`='{$sgin_md5all}' ");

    $data = "<?xml version=\"1.0\" encoding=\"utf-8\"?><response><data>{$data}</data><code>{$code}</code><date>" . PLUG_DATE() . "</date><unix>" . PLUG_UNIX() . "</unix><microtime>" . $microtime . "</microtime><appsafecode>" . Plug_Set_Data('appsafecode') . "</appsafecode><sgin>" . $sgin_md5 . "</sgin><sgin2>" . $sgin_md52 . "</sgin2></response>";

    return ($data);
}
