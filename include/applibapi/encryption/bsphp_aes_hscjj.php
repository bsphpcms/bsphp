<?php
function hex_ToStr_($hex)
{
$str="";
for ($i=0; $i < strlen($hex) - 1; $i +=2)
$str .=chr(hexdec($hex[$i] . $hex[$i + 1]));
return $str;
}
function String2Hex($string)
{
$hex='';
for ($i=0; $i < strlen($string); $i++) {
$hex .=dechex(ord($string[$i]));
}
return $hex;
}
function bsphp_aes_hscjj_bsphp_mdecrypt($encryptStr, $key)
{
$localIV=substr(md5($key), 8, 16);
$encryptKey_2=md5($key);
$encryptedData=hex_ToStr_($encryptStr);
for ($i=15; $i >=0; $i--) {
$key_2=$key . $i . $encryptKey_2;
$keytemp=substr(md5($key_2), 8, 16);
$encryptedData=openssl_decrypt($encryptedData, 'AES-128-CBC', $keytemp, OPENSSL_RAW_DATA, $localIV);
}
return $encryptedData;
}
function bsphp_aes_hscjj_bsphp_mencrypt($encryptStr, $key)
{
$localIV=substr(md5($key), 8, 16);
$encryptKey_2=md5($key);
$encrypted=$encryptStr;
for ($i=0; $i <=15; $i++) {
$key_2=$key . $i . $encryptKey_2;
$keytemp=substr(md5($key_2), 8, 16);
$encrypted=openssl_encrypt($encrypted, 'AES-128-CBC', $keytemp, OPENSSL_RAW_DATA, $localIV);
}
return urlencode(base64_encode($encrypted));
}
