<?php


function bsphp_des_python_bsphp_mdecrypt($param_encrypted, $param_key)
{



	if(PHP_VERSION>=7){
    
    
    $param_encrypted = base64_decode($param_encrypted);
    $param_key = substr(md5($param_key), 0, 8);
    $param_decrypted = openssl_decrypt($param_encrypted, 'DES-ECB', $param_key,  OPENSSL_NO_PADDING); 

    return $param_decrypted;
    
    
    	  
   }else{

    //$param_encrypted = hexToStr($param_encrypted);
    $param_encrypted = base64_decode($param_encrypted);
    $param_key = substr(md5($param_key), 0, 8);
    $param_td = mcrypt_module_open(MCRYPT_DES,'',MCRYPT_MODE_ECB,''); //使用MCRYPT_DES算法,cbc模式
    //$param_td = mcrypt_module_open('tripledes', '', MCRYPT_MODE_ECB, ''); //使用MCRYPT_DES算法,cbc模式
    $param_iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($param_td), MCRYPT_RAND);
    $param_ks = mcrypt_enc_get_key_size($param_td);
    mcrypt_generic_init($param_td, $param_key, $param_key); //初始处理
    $param_decrypted = mdecrypt_generic($param_td, $param_encrypted);
    mcrypt_generic_deinit($param_td); //结束
    mcrypt_module_close($param_td);
    //$param_y = pkcs5_unpad($param_decrypted);
    return $param_decrypted;
    
    
   }
}


/**
 * @DES 加密
 * 
 * 
 * 参数1=密码
 * 参数2=加密文本
 * 
 * 
 */
function bsphp_des_python_bsphp_mencrypt($param_text, $param_key)
{
    
    
    if(PHP_VERSION>=7){
        
     $param_key = substr(md5($param_key), 0, 8);
     $param_y=pkcs5_pad($param_text); 
     //$param_y = $param_text;
     $param_decrypted = openssl_encrypt($param_y, 'DES-ECB', $param_key,  OPENSSL_NO_PADDING);
     return base64_encode($param_decrypted);
        
    }else{

    //密码MD5加密取左边8位
    $param_key = substr(md5($param_key), 0, 8);
    $param_y = pkcs5_pad($param_text);
    //$param_y = $param_text;
    //$param_td = mcrypt_module_open(MCRYPT_DES,'',MCRYPT_MODE_ECB,''); //使用MCRYPT_DES算法,cbc模式ECB
    $param_td = mcrypt_module_open('tripledes', '', MCRYPT_MODE_ECB, ''); //使用MCRYPT_DES算法,cbc模式
    // $param_iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($param_td), MCRYPT_RAND);
    $param_ks = mcrypt_enc_get_key_size($param_td);
    mcrypt_generic_init($param_td, $param_key, $param_key); //初始处理
    $param_encrypted = mcrypt_generic($param_td, $param_y); //解密
    mcrypt_generic_deinit($param_td); //结束
    mcrypt_module_close($param_td);
    return base64_encode($param_encrypted);
    //return strToHex($param_encrypted);
    
    }
}





?>