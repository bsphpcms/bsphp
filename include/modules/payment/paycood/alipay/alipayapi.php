<?php
header("Content-type:text/html;charset=utf-8");


//文件说明 2022.2.18 小周
//这个是支付的入口文件
//调用支付时候会在配置文件种调用本文件,会传递一个变量进来！
//form_config.php 设置的open.php 为入口
//我们在这里判断有没开启微信支付。和后台支付方式二维码还是jsapi 
//$__GLOBAL__PAY__  来源系统载入open时候传递的
//记得配置服务器地址 ，后台系统配置=>服务器地址

//接受来宇宙的力量
//print_r($__GLOBAL__PAY__);
$pay_order     =   $__GLOBAL__PAY__[0];//订单 注意:订单号BUYVIP 开头为直接续费的
$pay_beizhu =   $__GLOBAL__PAY__[1];//标题
$pay_amount =   $__GLOBAL__PAY__[2];//金额

if(PHP_VERSION < 5.6){
	echo '支付宝要求php版本不能小5.6,你当前php版本'.PHP_VERSION;
	exit;
}


if(plug_get_configs_value('pay_alipay','pay_alipay_set')==1){
	echo '没有开启支付宝支付';
	exit;
}



//检查所有配置
if(plug_get_configs_value('pay_alipay','pay_alipay_id')==''){
    echo ' appid 没有配置';
	exit;	
}
if(plug_get_configs_value('pay_alipay','pay_alipay_key')==''){
    echo '后台支付宝参数 没有配置';
	exit;	
}

if(plug_get_configs_value('pay_alipay','pay_alipay_user')==''){
    echo '后台支付宝参数 没有配置';
	exit;	
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>支付</title>
</head>
<?php




require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/pagepay/service/AlipayTradeService.php';
require_once dirname(__FILE__).'/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';

    //商户订单号，商户网站订单系统中唯一订单号，必填
    $out_trade_no = trim($pay_order);

    //订单名称，必填
    $subject = trim($pay_beizhu);

    //付款金额，必填
    $total_amount = trim($pay_amount);

    //商品描述，可空
    $body = trim('无');

	//构造参数
	$payRequestBuilder = new AlipayTradePagePayContentBuilder();
	$payRequestBuilder->setBody($body);
	$payRequestBuilder->setSubject($subject);
	$payRequestBuilder->setTotalAmount($total_amount);
	$payRequestBuilder->setOutTradeNo($out_trade_no);

	$aop = new AlipayTradeService($config);

	/**
	 * pagePay 电脑网站支付请求
	 * @param $builder 业务参数，使用buildmodel中的对象生成。
	 * @param $return_url 同步跳转地址，公网可以访问
	 * @param $notify_url 异步通知地址，公网可以访问
	 * @return $response 支付宝返回的信息
 	*/
	$response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);

	//输出表单
	//var_dump($response);
?>
</body>
</html>