<?php



    //-----------------------------------------页面头部------------------------------------------------
    print <<< EOT

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bsphp-PRO软件管理系统安装向导-Install</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrap">
    <div class="container">
        <div id="install">
            <div class="top">
                <div class="version">
                    <dl>
                        <dt class="current">当前版本：</dt>
                        <dd>PRO</dd>
                    </dl>
                </div>
            </div>
            <div class="con">

EOT;
    if (empty($step))
    {
        //-----------------------------------------打印安装协议------------------------------------------------
        if (!isset($check))
        {
            print <<< EOT

            <div class="box3">
                <div id="error">
                    <div class="box4">
                        <img src="images/icon_06.gif" alt="错误" /><br />
                        <strong class="red">出错啦！请正确运行安装.</strong><br />
                        <strong class="black"></strong>
                    </div>
                </div>
            </div>

EOT;
        } else
        {
            print <<< EOT

<div class="box2">
                    <div class="box2-top">
                        <h2>请阅读 Bsphp软件管理验证系统 安装协议</h2>
                    </div>
                    <div class="box2-con">
                      <div class="agreement">
        <h3 align="center">Bsphp-PRO软件收费验证系统　使用协议</h3>
        <p align="center">&nbsp;</p>
        <p align="left">我们是《BSPHP技术团队》在以下协议简称:我们</p>
        <p align="left">Bsphp-PRO软件收费验证系统是我们旗下团队开发一款产品,使用前请请认真阅读使用条款！</p>
        <p align="left">感谢您使用Bsphp-PRO,希望我们的努力能为您提供一个高效的客户端管理方案。        </p>
        <p align="left">BSPHP系统版权属于我们所有,所有的使用者禁止以任何新形式修改发布衍生新版本并且贩卖,BSPHP-PRO系统已经在国家版权局申请版权,如有违约者必追究一切法律责任与赔偿。          </p>
        <p align="left">在使用BSPHP-PRO系统期间所产生的任何法律责任自行处理,且请勿用于非法用途否则一切后果自行承担,与我们无关。</p>
        <p align="left">环境安全协议,我们只提供BSPHP系统是授权使用,BSPHP-PRO安装使用环境与安全由用户自由支配,因为安全问题引起一切后果以及数据损失由用户自行承担,我们不承担任何责任。</p>
        <p align="left">使用须知用户出于自愿而使用本BSPHP系统，您必须了解使用本系统的风险，在尚未购买产品与技术服务之前，我们不承诺提供任何形式的技术支持、使用维护担保，也不承担任何因使用本系统而产生问题的相关责任。</p>
        <p align="left">禁止在 BSPHP-PRO系统 的整体或任何部分基础上以发展任何派生版本、修改版本或第三方版本用于重新分发,如果您未能遵守本协议的条款，您的授权将被终止，所被许可的权利将被收回，并承担相应法律责任。</p>
        <p align="left">你同意以上内容点击同意并且继续安装,否则点击拒绝 </p>
        <p align="left">安装使用BSPHP-PRO系统使用起等同协议生效</p>
        <p align="left">&nbsp;</p>

                      </div>
                    </div>
                    <div class="box2-fot">
                        <div class="handle">
                            <form method="get" action=''>
                                <input type="hidden" name="step" value="1">
                                <button type="submit" class="button" >同&nbsp;&nbsp;意</button> <button type="button" class="button" onclick="javascript: window.close();">拒&nbsp;&nbsp;绝</button>
                            </form>
                        </div>
                    </div>
              </div>

EOT;
        }
    } elseif ($step == 1)
    {
	
	
	
	$PHP_V=PHP_VERSION;
	
	if(PHP_VERSION >= 5.3 AND PHP_VERSION <= 9.0){
	  
	  $PHP_V_HTML='<span style="color:#0C6; font-weight:bold;">&radic; 环境可用</span>';
	}ELSE{
	    
	    if(PHP_VERSION > 8.0){
	        $PHP_V_HTML='<span style="color:#F00; font-weight:bold;">环境可用,建议低或9.0版本</span>';
	    }
	    
	  
	  
	}
	
	
	/*
	
	
	$able=get_loaded_extensions();
	if(in_array('Zend Guard Loader',$able)){
	   $ZEND_HTML='<span style="color:#0C6; font-weight:bold;">&radic; 环境可用</span>';
	}else{
	   $ZEND_HTML='<span style="color:#F00; font-weight:bold;">组件不支持</span>';
	    $url1='#';
	
	}
    <tr>
      <th>Zend Guard Loader组件</th>
      <td>{$ZEND_HTML}</td>
    </tr>
	
    */


        //-----------------------------------------安装第1步，检测属性------------------------------------------------
        print <<< EOT

<div class="box3">
  <h2>系统使用环境监测</h2>
  <table>
    <tr>
      <th width="455">名称</th>
      <td width="147"><span style="color:#07519A; font-weight:bold;">状 态</span></td>
    </tr>
	
    <tr>
      <th>当前PHP版本:{$PHP_V}</th>
      <td>{$PHP_V_HTML}</td>
    </tr>
	
    




EOT;
            $report_txt = array(1 =>
                '<span style="color:#0C6; font-weight:bold;">&radic; 正常可写</span>', 2 =>
                '<span style="color:#F00; font-weight:bold;">X 不可写</span>', 3 =>
                '<span style="color:#F00; font-weight:bold;">X 不存在</span>', );

            foreach ($file_check_report as $report)
            {
			
		
                print <<< EOT

			
    <tr>
      <th>目录:$report[0]&nbsp; </th>
      <td>{$report_txt[$report[1]]}</td>
    </tr>

EOT;
            }

            print <<< EOT
            
            

          <tr>
      <th>提示不要去想跳过这步,因为跳过无益！</th>
      <td></td>
    </tr>

  
    </table>
  
    
 <p>
  <div class="handle">
                
                    <button type="button" class="button" onclick="window.location.href='install.php?check=1'">上一步</button> 
					<button  type="submit" class="button" onclick="window.location.href='$url1'" >下一步</button>
              
      </div>
   </div>
  


  <div style="border-bottom:1px solid #DFECF7;"></div>
        
EOT;
         //-----------------------------------------安装第2步，安装方式选择------------------------------------------------
    }elseif($step==2){

        print <<< EOT
        
           
   <div id="step">
                    <div id="step2">
                        第二步
                    </div>
                    <form method="post" action=''>
                    
                    <div id="database">
                        <div class="box3">
                            <h2>填写数据库信息</h2>


                            <input type="hidden" name="step" value="4">
                            <table>
                                <tr>
                                    <th width="106"><div align="right">数据库地址：</div></th>
                                    <td width="180"><input type="text" class="int" name='dbhost' value='$param_dbhost' /></td>
                                    <td>如：127.0.0.1</td>
                                </tr>
                                <tr>
                                    <th><div align="right">数据库用户名：</div></th>
                                    <td><input type="text" class="int" name='dbuser' value='$param_dbuser' /></td>
                                    <td>输入MYSQL登录账号</td>
                                </tr>
                                <tr>
                                    <th><div align="right">数据库密码：</div></th>
                                    <td><input type="password" class="int" name='dbpw' value='$param_dbpw' /></td>
                                    <td>输入MYSQL登录密码</td>
                                </tr>

                                <tr>
                                    <th><div align="right">数据库名：</div></th>
                                    <td><input type="text" class="int" name='dbname' value='$param_dbname'/></td>
                                    <td>输入<strong  >已存在的</strong>数据库名称</td>
                                </tr>
                                <tr>
                                  <th><div align="right">数据表缓：</div></th>
                                  <td><input type="text" class="int" name='dbtop' value='$param_dbtop'/></td>
                                  <td>一个数据库安装多个版本需要更改</td>
                                </tr>
     


                                <tr>
                                <td colspan='3'>                                 </td>
                                </tr>
                            </table>

                        </div><!--/ box3-->

                      </div>
                <div class="handle">


                    <button type="button" onclick="window.location.href='install.php?step=1'" class="button">上一步</button> 
					<button type="submit" id="submit" name="submit" class="button">下一步</button>
                </div>



  
EOT;
    }elseif($step==5){
        print <<< EOT
        
           
  
                        <div id="step">
            <div id="step4">
                第四步
            </div>
            <div class="box3">
                <div id="error">
                    <div class="box4">
                        <img src="images/icon_04.gif" alt="成功" /><br />
                        <strong class="black">成功安装BSPHP-PRO软件管理系统， <font color="Red"><a href="../admin">点击进入后台</a></font>
                    </div>
                </div>
            </div>

  
EOT;
}elseif($step==3){
        print <<< EOT
        
           
  
                        <div id="step">
            <div id="step1">
                第四步
            </div>
            <div class="box3">
                <div id="error">
                    <div class="box4">
                        <img src="images/icon_04.gif" alt="成功" /><br />
                        <strong class="black">BSPHP-PRO软件管理系统已经安装过了</strong>
						<br/>
						如果需要重新安装请选删除根目录下 <strong class="black">Data/install.txt</strong> 文件
                    </div>
                </div>
            </div>

  
EOT;
}
    //-----------------------------------------页面底部------------------------------------------------
    print <<< EOT

               <div id="foot">Copyright 2009-2022 BSPHP技术团队  <a href="http://www.bsphp.com" target="_blank"> Bsphp-PRO验证系统  <?php echo BSPHP_VERSION; ?></a> Bsphp.com <br>
  All Rights Reserved </div>
            </div>
            <div class="fot"></div>
        </div>
    </div>
</div>
</body>
</html>
EOT;
?>
