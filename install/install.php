<?php
@header("content-Type: text/html; charset=utf-8"); //语言强制
$lujing = str_replace('install', '', dirname(__file__));

error_reporting(E_ALL);
/**
 * 转化 \ 为 /
 * 
 * @param	string	$param_path	路径
 * @return	string	路径
 */
function dir_path($param_path)
{
    $param_path = str_replace('\\', '/', $param_path);
    if (substr($param_path, -1) != '/')
        $param_path = $param_path . '/';
    return $param_path;
}

/**
 * 创建目录
 * 
 * @param	string	$param_path	路径
 * @param	string	$mode	属性
 * @return	string	如果已经存在则返回true，否则为flase
 */
function dir_create($param_path, $mode = 0777)
{
    if (is_dir($param_path))
        return true;
    $ftp_enable = 0;
    $param_path = dir_path($param_path);
    $temp = explode('/', $param_path);
    $cur_dir = '';
    $max = count($temp) - 1;
    for ($i = 0; $i < $max; $i++) {
        $cur_dir .= $temp[$i] . '/';
        if (@is_dir($cur_dir))
            continue;
        @mkdir($cur_dir, 0777, true);
        @chmod($cur_dir, 0777);
    }
    return is_dir($param_path);
}

/**
 * 拷贝目录及下面所有文件
 * 
 * @param	string	$fromdir	原路径
 * @param	string	$todir		目标路径
 * @return	string	如果目标路径不存在则返回false，否则为true
 */
function dir_copy($fromdir, $todir)
{
    $fromdir = dir_path($fromdir);
    $todir = dir_path($todir);
    if (!is_dir($fromdir))
        return false;
    if (!is_dir($todir))
        dir_create($todir);
    $list = glob($fromdir . '*');
    if (!empty($list)) {
        foreach ($list as $v) {
            $param_path = $todir . basename($v);
            if (is_dir($v)) {
                dir_copy($v, $param_path);
            } else {
                copy($v, $param_path);
                @chmod($param_path, 0777);
            }
        }
    }
    return true;
}





/**
 * @取得当前URL路径
 * 
 * 用了协助信息的返回
 */
function url()
{

    $pageURL = 'http';
    if (@$_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }


    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}



/**
 * *******************************
 * 获取随机数
 * 
 * 参数1=随机数长度
 * 
 */
function call_my_gte_test_arr($int)
{

    /**********取随机数字数组**************/
    $check_list = array(
        'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'l', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'z', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'I', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', 1, 2, 3, 4, 5, 6, 7,
        8, 9, 0
    );

    $test = '';
    shuffle($check_list); //打乱
    $ki = 0;
    while ($ki < $int) {
        $test .= $check_list[mt_rand(1, 61)];
        $ki++;
    }
    return $test;
}

/**
 * 判断 文件/目录 是否可写（取代系统自带的 is_writeable 函数）
 *
 * @param string $file 文件/目录
 * @return boolean
 */
function new_is_writeable($file)
{
    if (is_dir($file)) {
        $dir = $file;
        if ($fp = @fopen("$dir/test.txt", 'w')) {
            @fclose($fp);
            @unlink("$dir/test.txt");
            $writeable = 1;
        } else {
            $writeable = 0;
        }
    } else {
        if ($fp = @fopen($file, 'a+')) {
            @fclose($fp);
            $writeable = 1;
        } else {
            $writeable = 0;
        }
    }

    return $writeable;
}


/**
 * @信息框系统框
 * 默认返回前一页
 */
function alert($NAME)
{
    echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
    echo "<script language=javascript>alert('" . $NAME . "');</script>";
    echo '<script> var url =document.referrer; window.location=url; </script> ';
    exit;
}

isset($_GET['check']) ? $check = $_GET['check'] : $check = null;
isset($_GET['step']) ? $step = $_GET['step'] : $step = null;

/**
 * 判断是否安装过
 */


if (file_exists($lujing . 'Data/install.txt') & $step != 3) {
    if ($step != 5) exit('<meta http-equiv="refresh" content="0;URL=install.php?step=3" />');
}





isset($_POST['dbhost']) ? $param_dbhost = $_POST['dbhost'] : $param_dbhost = '127.0.0.1';
isset($_POST['dbuser']) ? $param_dbuser = $_POST['dbuser'] : $param_dbuser = 'root';
isset($_POST['dbpw']) ? $param_dbpw = $_POST['dbpw'] : $param_dbpw = '';
isset($_POST['dbname']) ? $param_dbname = $_POST['dbname'] : $param_dbname = '';
isset($_POST['dbtop']) ? $param_dbtop = $_POST['dbtop'] : $param_dbtop = 'bsphp_';
isset($_POST['Coding']) ? $Coding = $_POST['Coding'] : $Coding = '0';




$file_check_error = 1;
if ($step == 1) //----------------------------------------系全新检测------------------------------------------
{
    $report = array();


    include_once('data/fi_array.php');
    $ok = 0;
    $url1 = 'install.php?step=1';
    $a = count($arr_Afig);
    $i = 1;
    while ($i <= $a) {
        $file_check_report[$i][0] = $lujing . $arr_Afig[$i];
        $filename = $lujing . $arr_Afig[$i];
        if (file_exists($filename)) {
            if (new_is_writeable($filename)) {
                $file_check_report[$i][1] = '1';
                $filename_ok = '1';

                if ($ok == 0) $ok = 2;
            } else {
                $file_check_report[$i][1] = '2';
                $ok = 1;
            }
        } else {
            $file_check_report[$i][1] = '3';
            $ok = 1;
        }
        $i++;
    }

    if ($ok == 2) $url1 = 'install.php?step=2';
} elseif ($step == 2)
//---------------------------------------------安装数据库升级的和全新的---------------------------------------
{



    if (isset($_POST['submit'])) {

        if (PHP_VERSION >= 5.3 and PHP_VERSION < 5.4) {
            $file_dir = '5.3';
        } elseif (PHP_VERSION >= 5.4 and PHP_VERSION < 5.5) {
            $file_dir = '5.4';
        } elseif (PHP_VERSION >= 5.5 and PHP_VERSION < 7.0) {
            $file_dir = '5.5+';
        } elseif (PHP_VERSION >= 7.0 and PHP_VERSION < 7.2) {
            $file_dir = '7.0+';
        } elseif (PHP_VERSION >= 8.0 and PHP_VERSION < 8.9) {
            $file_dir = '8.0+';
        } else {
            echo 'PHP环境不正确,请尝试php5+/7+/8+环境,安装不能继续因为无法使用！';
            //exit;
        }


        /**
         * @拷贝文件
         */
        //dir_copy('PhpFile/LibBsphp'.$file_dir,'../LibBsphp/');
        //dir_copy('PhpFile/modules'.$file_dir,'../include/modules/');




        $param_array = explode(":", $param_dbhost);

        $port = 3306;
        if (isset($param_array[1])) {
            $port = $param_array[1];
        }

        try {
            // 假设 $mysqli 是一个已经连接的 mysqli 对象
            $link = mysqli_connect($param_array[0], $param_dbuser, $param_dbpw, $param_dbname, $port) or die('数据库连接失败 IP地址、用户名或者密码错误 MYSQL错误信息:' . mysqli_error($link));
        } catch (mysqli_sql_exception $e) {
            // 输出错误信息和代码

            echo "Error: 数据库连接失败 IP地址、用户名或者密码错误 MYSQL错误信息 " . $e->getMessage() . " (Code: " . $e->getCode() . ") SQL:" . mysqli_error($link);
            // 如果需要，可以记录错误到日志
            error_log("Error: " . $e->getMessage() . "SQL:" . mysqli_error($link));
        }

        mysqli_query($link, "set names 'utf8'");



        /**
         * 写出配置文件
         */
        $ini = '<?php 
     //以下是MYSQL配置信息,迁移时只需要配置好以下信息即可
	 //南宁火蝶科技有限公司
	 //Bsphp Inc
     const   DBHOST = "' . $param_dbhost . '";     //数据库服务器地址 127.0.0.1
     const   DBUSER = "' . $param_dbuser . '";     //数据库用户名
     const   DBPASS = "' . $param_dbpw . '";       //数据库密码
     const   DBTABLE = "' . $param_dbname . '";    //数据库名
     const   DBQIANHUAN = "' . $param_dbtop . '";        //表格缓 
     ?>';
        $param_tmp = file_put_contents('../Data/Bsmysql.Config.php', $ini);
        if (!$param_tmp) die('配置保存失败,/Data/Bsmysql.Config.php 不可写,请设置属性777可写,有相关疑问到 CHM.BSPHP.COM 查询!,');



        /**
         * 读取数据库
         * 
         * AppEn.mysql.txt=结构
         */
        //结构
        $param_sql = file_get_contents('data/AppEn.mysql.txt');
        $param_sql = str_replace('bs_php_', $param_dbtop, $param_sql);






        $param_sqlarr = explode(";", $param_sql);


        //数据
        $param_sql = file_get_contents('data/mysql.data.txt');
        $param_sql = str_replace('bs_php_', $param_dbtop, $param_sql);

        $param_sql = str_replace('BSPHP_HOSY_PASSWORD', 'Bsphp_' . call_my_gte_test_arr(10), $param_sql);
        $param_sql = str_replace('BSPHP_HOSY_COOKIES', 'Bsphp_' . call_my_gte_test_arr(10), $param_sql);

        $BSPHP_HOSY_HOST_URL = str_replace('install/install.php?step=2', '', strtolower(url()));
        $param_sql = str_replace('BSPHP_HOST_URL', $BSPHP_HOSY_HOST_URL, $param_sql);

        $dataarr = explode(";", $param_sql);

        for ($i = 0; $i < count($param_sqlarr) - 1; $i++) {

            //echo $param_sqlarr[$i] . "<br/>";
            if ($param_sqlarr[$i] != '') {




                try {
                    // 假设 $mysqli 是一个已经连接的 mysqli 对象
                    $result = mysqli_query($link, $param_sqlarr[$i]);
                } catch (mysqli_sql_exception $e) {
                    // 输出错误信息和代码

                    #echo "Error: " . $e->getMessage() . " (Code: " . $e->getCode() . ") SQL:" . $param_sqlarr[$i];
                    // 如果需要，可以记录错误到日志
                    error_log($e->getMessage() . "SQL:" . $param_sqlarr[$i]);
                }
            }
        }

        for ($i = 0; $i < count($dataarr) - 1; $i++) {

            //echo $param_sqlarr[$i] . "<br/>";
            if ($dataarr[$i]) {


                try {
                    // 假设 $mysqli 是一个已经连接的 mysqli 对象
                    $result =  mysqli_query($link, $dataarr[$i]);
                } catch (mysqli_sql_exception $e) {
                    // 输出错误信息和代码

                    #echo "Error: " . $e->getMessage() . " (Code: " . $e->getCode() . ") SQL:" . $param_sqlarr[$i];
                    // 如果需要，可以记录错误到日志
                    error_log($e->getMessage() . "SQL:" . $dataarr[$i]);
                }
            }
        }



        $param_tmp = file_put_contents($lujing . 'Data/install.txt', 'ok');
        echo '<meta http-equiv="refresh" content="0;URL=install.php?step=5" />';
    }
} elseif ($step == 3)
//---------------------------------------------安装数据库升级的和全新的---------------------------------------
{

    if (!file_exists($lujing . 'Data/install.txt')) {
        if ($step != 5) exit('<meta http-equiv="refresh" content="0;URL=index.php" />');
    }
}
include_once('template/install.php');
