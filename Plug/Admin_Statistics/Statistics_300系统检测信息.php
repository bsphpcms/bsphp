<?php


$tody_date = date('Y-m-d', PLUG_UNIX()); //日期1
$snutady = date('w', PLUG_UNIX()); //星期几2
$tody_unix = PLUG_UNIX(); //时间3
$log_info_array = array();



$param_date = strtotime(date('Y-m-d 00:00:00'));
$param_sql = "select count(*)as'hangshu'from`bs_php_log`WHERE  leixing = 'exit_log' and  `date` > {$param_date} ";
$param_tmp_arr = Plug_Query_Assoc($param_sql);
$param_log_info_array["" . 'logday'] = (int)$param_tmp_arr["" . 'hangshu']; 

$param_date = strtotime(date('Y-m-d 00:00:00', PLUG_UNIX() - 604800));
$param_sql = "select count(*)as'hangshu'from`bs_php_log`WHERE  leixing = 'exit_log' and  `date` > {$param_date} ";
$param_tmp_arr = Plug_Query_Assoc($param_sql);
$param_log_info_array["" . 'logday7'] = (int)$param_tmp_arr["" . 'hangshu']; 


$param_date = strtotime(date('Y-m-d 00:00:00'));
$param_sql = "select count(*)as'hangshu'from`plug_bsphp_api_dbug`WHERE  api = '' and  `time` > {$param_date} ";
$param_tmp_arr = Plug_Query_Assoc($param_sql);
$param_log_info_array["" . 'apiday'] = (int)$param_tmp_arr["" . 'hangshu']; 

$param_date = strtotime(date('Y-m-d 00:00:00', PLUG_UNIX() - 604800));
$param_sql = "select count(*)as'hangshu'from`plug_bsphp_api_dbug`WHERE  api = '' and  `time` > {$param_date} ";
$param_tmp_arr = Plug_Query_Assoc($param_sql);
$param_log_info_array["" . 'apiday7'] = (int)$param_tmp_arr["" . 'hangshu']; 



?>


<div class="layui-col-md12">
  <div class="layui-card">
    <div class="layui-card-header"><?php echo Plug_Lang('系统检测信息'); ?><span style="color: #d0d0d0;font-size: 9px;" > <?php echo Plug_Lang('二开修改'); ?>:Plug/Admin_Statistics/Statistics_300系统检测信息.php</span></div>
    <div class="layui-card-body">

      <div class="layui-carousel layadmin-carousel_m5 layadmin-carousel2 layadmin-carousel layadmin-backlog">
        <div carousel-item>
          <ul class="layui-row layui-col-space10">


            <li class="layui-col-md3">
              <a href="index.php?m=admin&c=log&a=table&t=exit_log" class="layadmin-backlog-body">
                <h3><?php echo Plug_Lang('今天系统安全防护日志'); ?></h3>
                <p><cite style="color: #FF5722;"><?PHP echo $param_log_info_array["" . 'logday']; ?><?php echo Plug_Lang('条'); ?></cite></p>
              </a>
            </li>
            <li class="layui-col-md3">
              <a href="index.php?m=admin&c=log&a=table&t=exit_log" class="layadmin-backlog-body">
                <h3><?php echo Plug_Lang('最近7天系统安全防护日志'); ?></h3>
                <p><cite style="color: #FF5722;"><?PHP echo $param_log_info_array["" . 'logday7']; ?><?php echo Plug_Lang('条'); ?></cite></p>
              </a>
            </li>


            <li class="layui-col-md3">
              <a href="index.php?m=dbug&c=log&a=table" class="layadmin-backlog-body">
                <h3><?php echo Plug_Lang('今日API接口空异常'); ?></h3>
                <p><cite style="color: #FF5722;"><?PHP echo $param_log_info_array["" . 'apiday']; ?><?php echo Plug_Lang('条'); ?></cite></p>
              </a>
            </li>
            <li class="layui-col-md3">
              <a href="index.php?m=dbug&c=log&a=table" class="layadmin-backlog-body">
                <h3><?php echo Plug_Lang('最近7天API接口空异常'); ?></h3>
                <p><cite style="color: #FF5722;"><?PHP echo $param_log_info_array["" . 'apiday7']; ?><?php echo Plug_Lang('条'); ?></cite></p>
              </a>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </div>
</div>


<style>
    .layadmin-carousel_m5 {
        background-color: rgb(255, 255, 255);
        height: 105px !important;
    }
</style>