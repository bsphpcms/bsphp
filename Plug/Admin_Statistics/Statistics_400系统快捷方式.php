<div class="layui-col-md6">
        <div class="layui-card">
          <div class="layui-card-header"><?php echo Plug_Lang('系统快捷方式'); ?><span style="color: #d0d0d0;font-size: 9px;" > <?php echo Plug_Lang('二开修改'); ?>:Plug/Admin_Statistics/Statistics_400系统快捷方式.php</span></div>
          <div class="layui-card-body" style="height: 210px!important;">

            <div class="layui-carousel layadmin-carousel layadmin-shortcut" style="height: 210px!important;">
              <div carousel-item>
                <ul class="layui-row layui-col-space10">

                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=applib&c=admin_app&a=add">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-release"> <?php echo Plug_Lang('添加软件'); ?></i>
                      <cite> </cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=applib&c=admin_app&a=table">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-app"> <?php echo Plug_Lang('软件列表'); ?></i>
                      <cite></cite>
                    </a>
                  </li>


                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=user&c=admin_user&a=table">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-user"> <?php echo Plug_Lang('用户账号'); ?></i>
                      <cite></cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=admin&c=root&a=table">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-console"> <?php echo Plug_Lang('后台账号'); ?></i>
                      <cite> </cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=admin&c=config&a=sys">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-component"> <?php echo Plug_Lang('系统配置'); ?></i>
                      <cite></cite>
                    </a>
                  </li>

                  <li class="layui-col-xs4">
                    <a lay-href="index.php?m=dbug&c=log&a=table">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-component"> <?php echo Plug_Lang('接口监控调试插件'); ?></i>
                      <cite></cite>
                    </a>
                  </li>


                </ul>

              </div>
            </div>

          </div>
        </div>
      </div>

      
      <div class="layui-col-md6">
        <div class="layui-card">
          <div class="layui-card-header"><?php echo Plug_Lang('官网快捷帮助'); ?><span style="color: #d0d0d0;font-size: 9px;" > <?php echo Plug_Lang('二开修改'); ?>:Plug/Admin_Statistics/Statistics_400系统快捷方式.php</span></div>
          <div class="layui-card-body" style="height: 210px!important;">

            <div class="layui-carousel1 layadmin-carousel layadmin-shortcut" style="height: 210px!important;">
              <div carousel-item>
                <ul class="layui-row layui-col-space10">

                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/sdk.html?chm">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('案例下载'); ?></i>
                      <cite> </cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/chm-0.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('API帮助'); ?></i>
                      <cite></cite>
                    </a>
                  </li>


                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/Plug.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('插件中心'); ?></i>
                      <cite></cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/chm-82.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('制卡教程'); ?></i>
                      <cite> </cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/sdk.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('对接教程'); ?></i>
                      <cite></cite>
                    </a>
                  </li>

                  <li class="layui-col-xs4">
                    <a lay-href="http://www.bsphp.com/chm-93.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('多开控制'); ?></i>
                      <cite></cite>
                    </a>
                  </li>

                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/chm-95.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('防劫持'); ?></i>
                      <cite></cite>
                    </a>
                  </li>
                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/chm-94.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('匿名API'); ?></i>
                      <cite></cite>
                    </a>
                  </li>

                  <li class="layui-col-xs4">
                    <a lay-href="https://www.bsphp.com/chm-145.html">
                      <i style="font-size: 1rem;" class="layui-icon layui-icon-survey"> <?php echo Plug_Lang('静态资源下载'); ?></i>
                      <cite></cite>
                    </a>
                  </li>
                </ul>

              </div>
            </div>
          </div>
        </div>
      </div>
