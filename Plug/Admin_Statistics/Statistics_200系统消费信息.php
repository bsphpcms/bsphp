<?php


$tody_date = date('Y-m-d', PLUG_UNIX()); //日期1
$snutady = date('w', PLUG_UNIX()); //星期几2
$tody_unix = PLUG_UNIX(); //时间3
$log_info_array = array();


/** 今天
 * @全部充值卡总金额   * @全部代理充值卡总价*/
$sql = "select count(*)as'hangshu',SUM(`car_Rmb`)as'car_Rmb',SUM(`car_DaoLi_Rmb`)as'car_DaoLi_Rmb'from`bs_php_cardseries`";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['ka_all'] = (int)$tmp_arr['hangshu']; //全部充值看数量
$log_info_array['ka_all_rmb'] = (int)$tmp_arr['car_Rmb']; //全部充值卡销售价格总和
$log_info_array['ka_all_d_rmb'] = (int)$tmp_arr['car_DaoLi_Rmb']; //全部代理充值卡价格总和



/** 今天
 * @全部充值卡总金额   * @全部代理充值卡总价*/
$sql = "select count(*)as'hangshu',SUM(`car_Rmb`)as'car_Rmb',SUM(`car_DaoLi_Rmb`)as'car_DaoLi_Rmb'from`bs_php_cardseries`";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['ka_all'] = (int)$tmp_arr['hangshu']; //全部充值看数量
$log_info_array['ka_all_rmb'] = (int)$tmp_arr['car_Rmb']; //全部充值卡销售价格总和
$log_info_array['ka_all_d_rmb'] = (int)$tmp_arr['car_DaoLi_Rmb']; //全部代理充值卡价格总和


/**
 * @已激活充值卡
 */
/**
 * @已激活充值卡销售价格总和 * @已激活充值卡代理价格销售总和*/
$sql = "select count(*)as'hangshu',SUM(`car_Rmb`)as'car_Rmb',SUM(`car_DaoLi_Rmb`)as'car_DaoLi_Rmb'from`bs_php_cardseries`WHERE`car_IsLock`='1'";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['ka_aut'] = (int)$tmp_arr['hangshu']; //全部已激活
$log_info_array['ka_aut_rmb'] = (int)$tmp_arr['car_Rmb']; //全部已激活销售价格总和
$log_info_array['ka_aut_d_rmb'] = (int)$tmp_arr['car_DaoLi_Rmb']; //全部已激活销代理格总和


/**
 * @未激活充值卡
 */
/**
 * @未激活充值卡销售价格总和 * @未激活充值卡代理价格销售总和*/
$sql = "select count(*)as'hangshu',SUM(`car_Rmb`)as'car_Rmb',SUM(`car_DaoLi_Rmb`)as'car_DaoLi_Rmb'from`bs_php_cardseries`WHERE`car_IsLock`='0'";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['ka_die'] = (int)$tmp_arr['hangshu']; //全部未激活
$log_info_array['ka_die_rmb'] = (int)$tmp_arr['car_Rmb']; //全部未激活销售价格总和
$log_info_array['ka_die_d_rmb'] = (int)$tmp_arr['car_DaoLi_Rmb']; //全部未激活销代理格总和



/**
 * @今天激活使用全部充值卡- 今天激活
 */
/** 今天
 * @今天全部制作充值卡总金额   * @今天全部制作代理充值卡总价*/
$sql = "select count(*)as'hangshu',SUM(`car_Rmb`)as'car_Rmb',SUM(`car_DaoLi_Rmb`)as'car_DaoLi_Rmb'from`bs_php_cardseries`WHERE`car_pur_date`>'{$tody_date}'AND`car_IsLock`='1'";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['ka_tady_out_all'] = (int)$tmp_arr['hangshu']; //全部充值看数量
$log_info_array['ka_tady_out_all_rmb'] = (int)$tmp_arr['car_Rmb']; //全部充值卡销售价格总和
$log_info_array['ka_tady_out_all_d_rmb'] = (int)$tmp_arr['car_DaoLi_Rmb']; //全部代理充值卡价格总和




/** 今天在线充值金额*/
$sql = "select count(`pay_rbm`)as'hangshu'from`bs_php_rmb_pay_log`WHERE `pay_zhuangtai`='0' and `pay_date`= '{$tody_date}'";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['user_tady_pay_rmb'] = (int)$tmp_arr['hangshu']; //今天在线充值金额



/** 今天消费够卡记录*/
$sql = "select SUM(`ka_shijia`)as'hangshu'from`bs_php_pay_log`WHERE`pay_date`>'{$tody_date}'AND`pay_zhuangtai`='0'";
$tmp_arr = Plug_Query_Assoc($sql);
$log_info_array['user_tady_buy_ka_rmb'] = (int)$tmp_arr['hangshu']; //今天消费够卡记录总金额



?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?php echo Plug_Lang('系统消费信息'); ?><span style="color: #d0d0d0;font-size: 9px;"> <?php echo Plug_Lang('二开修改'); ?>:Plug/Admin_Statistics/Statistics_200系统消费信息.php</span></div>
        <div class="layui-card-body">

            <div class="layui-carousel layadmin-carousel_m3 layadmin-carousel2 layadmin-carousel layadmin-backlog">


                <ul class="layui-row layui-col-space10">


                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天注册数量'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('总充值卡'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['ka_all']; ?><?php echo Plug_Lang('张'); ?></cite></p>
                        </a>
                    </li>
                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天注册数量'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('总未激活卡'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['ka_die']; ?><?php echo Plug_Lang('张'); ?></cite></p>
                        </a>
                    </li>
                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天注册数量'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('总已激活卡'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['ka_aut']; ?><?php echo Plug_Lang('张'); ?></cite></p>
                        </a>
                    </li>
                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天充值使用卡'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('今天激活卡'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['ka_tady_out_all']; ?><?php echo Plug_Lang('张'); ?></cite></p>
                        </a>
                    </li>
                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天消费余额'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('今天消费'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['user_tady_buy_ka_rmb']; ?><?php echo Plug_Lang('元'); ?></cite></p>
                        </a>
                    </li>


                    <li class="layui-col-md2">
                        <a href="javascript:;" onclick="layer.tips('<?php echo Plug_Lang('今天充值'); ?>', this, {tips: 3});" class="layadmin-backlog-body">
                            <h3><?php echo Plug_Lang('今天充值'); ?></h3>
                            <p><cite style="color: #FF5722;"><?PHP echo $log_info_array['user_tady_pay_rmb']; ?><?php echo Plug_Lang('元'); ?></cite></p>
                        </a>
                    </li>



                </ul>
            </div>

        </div>
    </div>
</div>
<style>
    .layadmin-carousel_m3 {
        background-color: rgb(255, 255, 255);
        height: 105px !important;
    }
</style>